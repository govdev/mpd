<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePemeriksaanNotarisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemeriksaan_notaris', function (Blueprint $table) {
            $table->id();
            $table->string('notaris_name');
            $table->string('notaris_alamat');
            $table->string('notaris_districts');
            $table->string('notaris_city');
            $table->string('notaris_province');
            $table->string('notaris_zipcode');
            $table->string('notaris_nilai');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pemeriksaan_notaris');
    }
}
