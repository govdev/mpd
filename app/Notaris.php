<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notaris extends Model
{
    protected $fillable = [
        'notaris_name',
        'notaris_alamat',
        'notaris_districts',
        'notaris_city',
        'notaris_province',
        'notaris_zipcode',
        'notaris_phone',
        'notaris_nilai',
       ];
}
