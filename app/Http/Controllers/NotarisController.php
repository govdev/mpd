<?php

namespace App\Http\Controllers;

use App\Notaris;
use Illuminate\Http\Request;
use Redirect;
use PDF;

class NotarisController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['notarises'] = Notaris::orderBy('created_at', 'desc')->paginate(10);

        return view('backend.notaris.index',$data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.notaris.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $totalNilai = $request['papan_nama']+$request['ruang_notaris']+$request['ruang_staff']+$request['ruang_tamu']+$request['spn']+$request['basjn']+$request['sc']+$request['skicn']+$request['kpa']+$request['siemon']+$request['minuta_akta']+$request['reportorium']+$request['buku_khusus']+$request['klapper']+$request['buku_protes']+$request['buku_wasiat']+$request['uji_petik']+$request['foto_presiden'];

        $request->validate([
            'notaris_name' => 'required|unique:notaris|max:255',
            'notaris_city' => 'required',
            'notaris_phone' => 'required',
            // 'notaris_nilai' => 'required',
        ]);
        // Notaris::create($request->all());
        Notaris::create([
            'notaris_name' => $request['notaris_name'],
            'notaris_city' => $request['notaris_city'],
            'notaris_phone' => $request['notaris_phone'],
            'notaris_nilai' => $totalNilai,
        ]);
        return Redirect::to('notaris')
       ->with('success','Greate! Notaris created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Notaris  $notaris
     * @return \Illuminate\Http\Response
     */
    public function show(Notaris $notaris)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Notaris  $notaris
     * @return \Illuminate\Http\Response
     */
    public function edit(Notaris $notaris)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Notaris  $notaris
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Notaris $notaris)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Notaris  $notaris
     * @return \Illuminate\Http\Response
     */
    public function destroy(Notaris $notaris)
    {
        //
    }
}
