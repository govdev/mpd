@extends('layouts.app')

@section('content')
<div class="auth-wrapper d-flex no-block justify-content-center align-items-center" style="background:url({{asset('images/background/login-register.jpg')}}) no-repeat center center; background-size: cover;">
            <div class="auth-box on-sidebar p-4 bg-white m-0 rounded">
                <div>
                    <div class="logo text-center">
                    <span class="db"><img src="{{asset('images/logo-icon.png')}}" alt="logo" /><br/>
                    <h4>Aplikasi Pemeriksaan Protokol Notaris</h4></span>
                    </div>
                    <h3 class="box-title mt-5 mb-0">Register Now</h3><small>Create your account and enjoy</small> 
                    <!-- Form -->
                    <div class="row">
                        <div class="col-12">
                            <form method="POST" action="{{ route('register') }}" class="form-horizontal mt-3 form-material">
                                     @csrf
                                <div class="form-group mt-3">
                                      <div class="col-xs-12">
                                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Name">

                                            @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                      </div>
                                    </div>
                                    <div class="form-group mb-3 ">
                                      <div class="col-xs-12">
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email">

                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                      </div>
                                    </div>
                                    <div class="form-group mb-3 ">
                                      <div class="col-xs-12">
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Password">
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                      </div>
                                    </div>
                                    <div class="form-group mb-3">
                                      <div class="col-xs-12">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Confirm Password">

                                      </div>
                                    </div>
                                    <div class="form-group mb-3">
                                      <div class="">
                                        <div class="checkbox checkbox-primary pt-0">
                                          <input id="checkbox-signup" type="checkbox" class="chk-col-indigo material-inputs">
                                          <label for="checkbox-signup"> I agree to all <a href="#">Terms</a></label>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="form-group text-center mt-3">
                                      <div class="col-xs-12">
                                        <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">{{ __('Register') }}</button>
                                      </div>
                                    </div>
                                    <div class="form-group mb-0">
                                      <div class="col-sm-12 text-center">
                                        <p>Already have an account? <a href="{{ url('login')}}" class="text-info ml-1">Login</a></p>
                                      </div>
                                    </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
@push('scripts')
    <script>
    $('[data-toggle="tooltip"]').tooltip();
    $(".preloader").fadeOut();
    </script>
@endpush