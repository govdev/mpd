@extends('layouts.app')

@section('content')

        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 col-12 align-self-center">
                    <h3 class="text-themecolor mb-0">Data Notaris</h3>
                    <ol class="breadcrumb mb-0 p-0 bg-transparent">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Data Notaris</li>
                    </ol>
                </div>

                @include('layouts.pagetitle')

            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- File export -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Data Notaris Telah di Periksa</h4>
                                <h6 class="card-subtitle">Daftar Notaris yang telah diperiksa oleh Majelis Pengawas Daerah Notaris Wilayah D.I. Yogyakarta</h6>
                                <div class="table-responsive">
                                    <table id="file_export" class="table table-striped table-bordered display">
                                        <thead>
                                            <tr>
                                                <th>Nama</th>
                                                <th>Kota</th>
                                                <!-- <th>Province</th> -->
                                                <!-- <th>No. Telepon</th> -->
                                                <th>Nilai</th>
                                                <th>Tanggal</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($notarises as $notaris)
                                        @isset($notaris->notaris_nilai) 
                                        
                                            @if($notaris->notaris_nilai>=81 && $notaris->notaris_nilai<=100)
                                                @php
                                                    $hasilNilai = "Sangat Baik";
                                                @endphp
                                             @elseif($notaris->notaris_nilai>=61 && $notaris->notaris_nilai<=80)
                                                @php
                                                    $hasilNilai = "Baik";
                                                @endphp
                                             @elseif($notaris->notaris_nilai>=41 && $notaris->notaris_nilai<=60)
                                                @php
                                                    $hasilNilai = "Cukup";
                                                @endphp
                                             @elseif($notaris->notaris_nilai>=21 && $notaris->notaris_nilai<=40)
                                                @php
                                                    $hasilNilai = "Kurang";
                                                @endphp
                                             @elseif($notaris->notaris_nilai>=1 && $notaris->notaris_nilai<=20)
                                                @php
                                                    $hasilNilai = "Buruk";
                                                @endphp
                                            @else
                                                @php
                                                    $hasilNilai = "Belum ada Nilai";
                                                @endphp
                                            @endif
                                            
                                        @endisset
                                        <tr>
                                            <td>{{ $notaris->notaris_name }}</td>
                                            <td>{{ $notaris->notaris_city }}</td>
                                            <!-- <td>{{ $notaris->notaris_province }}</td> -->
                                            <!-- <td>{{ $notaris->notaris_phone }}</td> -->
                                            <td>{{  $hasilNilai }} ({{$notaris->notaris_nilai}})</td>

                                            <td>{{ date('Y-m-d', strtotime($notaris->created_at)) }}</td>
                                            <!-- <td><a href="" class="btn btn-primary">Edit</a></td> -->
                               
                                        </tr>
                                        @endforeach

                                        </tbody>
     
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            @include('layouts.footer')

        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
@endsection
@push('head')
        <link href="{{ asset('libs/datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
@endpush
@push('scripts')
    <!--This page plugins -->
    <script src="{{asset('libs/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/pages/datatable/custom-datatable.js')}}"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
    <script src="{{asset('js/pages/datatable/datatable-advanced.init.js')}}"></script>
@endpush