@extends('layouts.app')

@section('content')
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 col-12 align-self-center">
                    <h3 class="text-themecolor mb-0">Pemeriksaan Protokol Notaris</h3>
                    <ol class="breadcrumb mb-0 p-0 bg-transparent">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{url('notaris')}}">Notaris</a></li>
                        <li class="breadcrumb-item active">Pemeriksaan Protokol Notaris</li>
                    </ol>
                </div>
                @include('layouts.pagetitle')

            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">

                    <div class="col-sm-12">
                        <form method="POST" action="{{ route('notaris.store') }}" class="form-horizontal mt-3 form-material">
                            @csrf
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Data Notaris</h4>
                                    <div class="form-group">
                                        <label for="notaris_name">Nama <span class="help"> e.g.
                                                "Amri Karisma S.Kom."</span></label>
                                        <input type="text" id="notaris_name" name="notaris_name"
                                            class="form-control" placeholder="Nama Notaris">
                                            <span class="text-danger">{{ $errors->first('notaris_name') }}</span>

                                    </div>
                                    <div class="form-group">
                                        <label for="notaris_city">Area Notaris <span class="help"> e.g.
                                                "BANTUL"</span></label>
                                        <input type="text" id="notaris_city" name="notaris_city"
                                            class="form-control" placeholder="Area Notaris">
                                            <span class="text-danger">{{ $errors->first('notaris_city') }}</span>

                                    </div>
                                    <div class="form-group">
                                        <label for="notaris_phone">No. Telepon <span class="help"> e.g.
                                                "087738990006"</span></label>
                                        <input type="number" id="notaris_phone" name="notaris_phone"
                                            class="form-control" placeholder="No. Telepon Notaris">
                                            <span class="text-danger">{{ $errors->first('notaris_phone') }}</span>

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Kondisi Kantor?</h4>
                                    <h5 class="card-title">Papan Nama</h5>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <input name="papan_nama"  class="material-inputs" type="radio" id="papan_nama_sangat_baik" value="5"/>
                                                <label for="papan_nama_sangat_baik">Sangat Baik</label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <input name="papan_nama" class="material-inputs" type="radio" id="papan_nama_baik" value="4"/>
                                                <label for="papan_nama_baik">Baik</label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                        <div class="form-group">
                                            <input name="papan_nama" class="material-inputs" type="radio" id="papan_nama_cukup" value="3"/>
                                            <label for="papan_nama_cukup">Cukup</label>
                                        </div>
                                        </div>
                                        <div class="col-md-2">
                                        <div class="form-group">
                                            <input name="papan_nama" class="material-inputs" type="radio" id="papan_nama_kurang" value="2" />
                                            <label for="papan_nama_kurang">Kurang</label>
                                        </div>
                                        </div>
                                        <div class="col-md-2">
                                        <div class="form-group">
                                            <input name="papan_nama" class="material-inputs" type="radio" id="papan_nama_buruk" value="1" />
                                            <label for="papan_nama_buruk">Buruk</label>
                                        </div>
                                        </div>

                            
                                    </div>
                                    <h5 class="card-title mt-3">Ruang Notaris</h5>
                                    <div class="row">
                                        <div class="col-md-2">
                                        <div class="form-group">
                                            <input name="ruang_notaris"  class="material-inputs" type="radio" id="ruang_notaris_sangat_baik" value="5"/>
                                            <label for="ruang_notaris_sangat_baik">Sangat Baik</label>
                                        </div>
                                        </div>
                                        <div class="col-md-2">
                                        <div class="form-group">
                                            <input name="ruang_notaris" class="material-inputs" type="radio" id="ruang_notaris_baik" value="4"/>
                                            <label for="ruang_notaris_baik">Baik</label>
                                        </div>
                                        </div>
                                        <div class="col-md-2">
                                        <div class="form-group">
                                            <input name="ruang_notaris" class="material-inputs" type="radio" id="ruang_notaris_cukup" value="3"/>
                                            <label for="ruang_notaris_cukup">Cukup</label>
                                        </div>
                                        </div>
                                        <div class="col-md-2">
                                        <div class="form-group">
                                            <input name="ruang_notaris" class="material-inputs" type="radio" id="ruang_notaris_kurang" value="2"/>
                                            <label for="ruang_notaris_kurang">Kurang</label>
                                        </div>
                                        </div>
                                        <div class="col-md-2">
                                        <div class="form-group">
                                            <input name="ruang_notaris" class="material-inputs" type="radio" id="ruang_notaris_buruk" value="1"/>
                                            <label for="ruang_notaris_buruk">Buruk</label>
                                        </div>
                                        </div>
                                    </div>
                                    <h5 class="card-title mt-3">Ruang Staff</h5>
                                    <div class="row">
                                        <div class="col-md-2">
                                        <div class="form-group">
                                            <input name="ruang_staff"  class="material-inputs" type="radio" id="ruang_staff_sangat_baik" value="5"/>
                                            <label for="ruang_staff_sangat_baik">Sangat Baik</label>
                                        </div>
                                        </div>
                                        <div class="col-md-2">
                                        <div class="form-group">
                                            <input name="ruang_staff" class="material-inputs" type="radio" id="ruang_staff_baik" value="4"/>
                                            <label for="ruang_staff_baik">Baik</label>
                                        </div>
                                        </div>
                                        <div class="col-md-2">
                                        <div class="form-group">
                                            <input name="ruang_staff" class="material-inputs" type="radio" id="ruang_staff_cukup" value="3"/>
                                            <label for="ruang_staff_cukup">Cukup</label>
                                        </div>
                                        </div>
                                        <div class="col-md-2">
                                        <div class="form-group">
                                            <input name="ruang_staff" class="material-inputs" type="radio" id="ruang_staff_kurang" value="2" />
                                            <label for="ruang_staff_kurang">Kurang</label>
                                        </div>
                                        </div>
                                        <div class="col-md-2">
                                        <div class="form-group">
                                            <input name="ruang_staff" class="material-inputs" type="radio" id="ruang_staff_buruk" value="1" />
                                            <label for="ruang_staff_buruk">Buruk</label>
                                        </div>
                                        </div>

                            
                                    </div>
                                    <h5 class="card-title mt-3">Ruang Tamu</h5>
                                    <div class="row">
                                        <div class="col-md-2">
                                        <div class="form-group">
                                            <input name="ruang_tamu"  class="material-inputs" type="radio" id="ruang_tamu_sangat_baik" value="5"/>
                                            <label for="ruang_tamu_sangat_baik">Sangat Baik</label>
                                        </div>
                                        </div>
                                        <div class="col-md-2">
                                        <div class="form-group">
                                            <input name="ruang_tamu" class="material-inputs" type="radio" id="ruang_tamu_baik" value="4"/>
                                            <label for="ruang_tamu_baik">Baik</label>
                                        </div>
                                        </div>
                                        <div class="col-md-2">
                                        <div class="form-group">
                                            <input name="ruang_tamu" class="material-inputs" type="radio" id="ruang_tamu_cukup" value="3" />
                                            <label for="ruang_tamu_cukup">Cukup</label>
                                        </div>
                                        </div>
                                        <div class="col-md-2">
                                        <div class="form-group">
                                            <input name="ruang_tamu" class="material-inputs" type="radio" id="ruang_tamu_kurang" value="2"/>
                                            <label for="ruang_tamu_kurang">Kurang</label>
                                        </div>
                                        </div>
                                        <div class="col-md-2">
                                        <div class="form-group">
                                            <input name="ruang_tamu" class="material-inputs" type="radio" id="ruang_tamu_buruk" value="1"/>
                                            <label for="ruang_tamu_buruk">Buruk</label>
                                        </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Surat Pengakatan Notaris</h4>
                                    <div class="row">
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="spn"  class="material-inputs" type="radio" id="spn_sangat_baik" value="5"/>
                                            <label for="spn_sangat_baik">Asli Ada di Kantor</label>
                                        </div>
                                        </div>
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="spn" class="material-inputs" type="radio" id="spn_baik" value="3" />
                                            <label for="spn_baik">Copy ada di Kantor</label>
                                        </div>
                                        </div>
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="spn" class="material-inputs" type="radio" id="spn_cukup" value="1"/>
                                            <label for="spn_cukup">Sudah Tidak Menyimpan</label>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Berita Acara Sumpah Jabatan Notaris</h4>
                                    <div class="row">
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="basjn"  class="material-inputs" type="radio" id="basjn_sangat_baik" value="5"/>
                                            <label for="basjn_sangat_baik">Asli Ada di Kantor</label>
                                        </div>
                                        </div>
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="basjn" class="material-inputs" type="radio" id="basjn_baik" value="3" />
                                            <label for="basjn_baik">Copy ada di Kantor</label>
                                        </div>
                                        </div>
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="basjn" class="material-inputs" type="radio" id="basjn_cukup" value="1"/>
                                            <label for="basjn_cukup">Sudah Tidak Menyimpan</label>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Sertifikat Cuti</h4>
                                    <div class="row">
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="sc"  class="material-inputs" type="radio" id="sc_sangat_baik" value="5"/>
                                            <label for="sc_sangat_baik">Asli Ada di Kantor</label>
                                        </div>
                                        </div>
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="sc" class="material-inputs" type="radio" id="sc_baik" value="3"/>
                                            <label for="sc_baik">Copy ada di Kantor</label>
                                        </div>
                                        </div>
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="sc" class="material-inputs" type="radio" id="sc_cukup" value="1"/>
                                            <label for="sc_cukup">Belum Memiliki</label>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Surat Keterangan Ijin Cuti Notaris</h4>
                                    <div class="row">
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="skicn"  class="material-inputs" type="radio" id="skicn_sangat_baik" value="5"/>
                                            <label for="skicn_sangat_baik">Asli Ada di Kantor</label>
                                        </div>
                                        </div>
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="skicn" class="material-inputs" type="radio" id="skicn_baik" value="3" />
                                            <label for="skicn_baik">Copy ada di Kantor</label>
                                        </div>
                                        </div>
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="skicn" class="material-inputs" type="radio" id="skicn_cukup" value="1"/>
                                            <label for="skicn_cukup">Belum Memiliki</label>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Keadaan Penyimpanan Arsip</h4>
                                    <div class="row">
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="kpa"  class="material-inputs" type="radio" id="kpa_sangat_baik" value="5"/>
                                            <label for="kpa_sangat_baik">Sangat Baik</label>
                                        </div>
                                        </div>
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="kpa" class="material-inputs" type="radio" id="kpa_baik" value="4"/>
                                            <label for="kpa_baik">Baik</label>
                                        </div>
                                        </div>
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="kpa" class="material-inputs" type="radio" id="kpa_cukup" value="3" />
                                            <label for="kpa_cukup">Cukup</label>
                                        </div>
                                        </div>
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="kpa" class="material-inputs" type="radio" id="kpa_kurang" value="2"/>
                                            <label for="kpa_kurang">Kurang</label>
                                        </div>
                                        </div>
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="kpa" class="material-inputs" type="radio" id="kpa_buruk" value="1" />
                                            <label for="kpa_buruk">Buruk</label>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Laporan SIEMON</h4>
                                    <div class="row">
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="siemon"  class="material-inputs" type="radio" id="siemon_sangat_baik" value="5"/>
                                            <label for="siemon_sangat_baik">Melaporkan Setiap Bulan</label>
                                        </div>
                                        </div>
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="siemon" class="material-inputs" type="radio" id="siemon_baik" value="3"/>
                                            <label for="siemon_baik">Melaporkan</label>
                                        </div>
                                        </div>
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="siemon" class="material-inputs" type="radio" id="siemon_cukup" value="1"/>
                                            <label for="siemon_cukup">Tidak Pernah Melaporkan</label>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Protokol Notaris</h4>
                                    <h5 class="card-title mt-3">Minuta Akta</h5>
                                    <div class="row">
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="minuta_akta"  class="material-inputs" type="radio" id="minuta_akta_sangat_baik" value="5"/>
                                            <label for="minuta_akta_sangat_baik">Sangat Baik</label>
                                        </div>
                                        </div>
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="minuta_akta" class="material-inputs" type="radio" id="minuta_akta_baik" value="3"/>
                                            <label for="minuta_akta_baik">Baik</label>
                                        </div>
                                        </div>
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="minuta_akta" class="material-inputs" type="radio" id="minuta_akta_cukup" value="1"/>
                                            <label for="minuta_akta_cukup">Cukup</label>
                                        </div>
                                        </div>
                                    </div>
                                    <h5 class="card-title mt-3">Buku daftar akta atau Reportorium</h5>
                                    <div class="row">
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="reportorium"  class="material-inputs" type="radio" id="reportorium_sangat_baik" value="5"/>
                                            <label for="reportorium_sangat_baik">Sangat Baik</label>
                                        </div>
                                        </div>
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="reportorium" class="material-inputs" type="radio" id="reportorium_baik" value="3"/>
                                            <label for="reportorium_baik">Baik</label>
                                        </div>
                                        </div>
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="reportorium" class="material-inputs" type="radio" id="reportorium_cukup" value="1"/>
                                            <label for="reportorium_cukup">Cukup</label>
                                        </div>
                                        </div>
                                    </div>
                                    <h5 class="card-title mt-3">Buku Khusus untuk mendaftarkan Surat dibawah Tangan</h5>
                                    <div class="row">
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="buku_khusus"  class="material-inputs" type="radio" id="buku_khusus_sangat_baik" value="5"/>
                                            <label for="buku_khusus_sangat_baik">Sangat Baik</label>
                                        </div>
                                        </div>
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="buku_khusus" class="material-inputs" type="radio" id="buku_khusus_baik" value="3"/>
                                            <label for="buku_khusus_baik">Baik</label>
                                        </div>
                                        </div>
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="buku_khusus" class="material-inputs" type="radio" id="buku_khusus_cukup" value="1"/>
                                            <label for="buku_khusus_cukup">Cukup</label>
                                        </div>
                                        </div>
                                    </div>
                                    <h5 class="card-title mt-3">Buku Daftar Nama Penghadap atau Klapper</h5>
                                    <div class="row">
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="klapper"  class="material-inputs" type="radio" id="klapper_sangat_baik" value="5"/>
                                            <label for="klapper_sangat_baik">Sangat Baik</label>
                                        </div>
                                        </div>
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="klapper" class="material-inputs" type="radio" id="klapper_baik" value="4"/>
                                            <label for="klapper_baik">Baik</label>
                                        </div>
                                        </div>
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="klapper" class="material-inputs" type="radio" id="klapper_cukup" value="3"/>
                                            <label for="klapper_cukup">Cukup</label>
                                        </div>
                                        </div>
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="klapper" class="material-inputs" type="radio" id="klapper_kurang" value="2"/>
                                            <label for="klapper_kurang">Kurang</label>
                                        </div>
                                        </div>
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="klapper" class="material-inputs" type="radio" id="klapper_buruk" value="1"/>
                                            <label for="klapper_buruk">Buruk</label>
                                        </div>
                                        </div>
                                    </div>
                                    <h5 class="card-title mt-3">Buku Daftar Protes</h5>
                                    <div class="row">
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="buku_protes"  class="material-inputs" type="radio" id="buku_protes_sangat_baik" value="5"/>
                                            <label for="buku_protes_sangat_baik">Sangat Baik</label>
                                        </div>
                                        </div>
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="buku_protes" class="material-inputs" type="radio" id="buku_protes_baik" value="4"/>
                                            <label for="buku_protes_baik">Baik</label>
                                        </div>
                                        </div>
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="buku_protes" class="material-inputs" type="radio" id="buku_protes_cukup" value="3"/>
                                            <label for="buku_protes_cukup">Cukup</label>
                                        </div>
                                        </div>
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="buku_protes" class="material-inputs" type="radio" id="buku_protes_kurang" value="2"/>
                                            <label for="buku_protes_kurang">Kurang</label>
                                        </div>
                                        </div>
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="buku_protes" class="material-inputs" type="radio" id="buku_protes_buruk" value="1"/>
                                            <label for="buku_protes_buruk">Buruk</label>
                                        </div>
                                        </div>
                                    </div>
                                    <h5 class="card-title mt-3">Buku Daftar Wasiat</h5>
                                    <div class="row">
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="buku_wasiat"  class="material-inputs" type="radio" id="buku_wasiat_sangat_baik" value="5"/>
                                            <label for="buku_wasiat_sangat_baik">Sangat Baik</label>
                                        </div>
                                        </div>
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="buku_wasiat" class="material-inputs" type="radio" id="buku_wasiat_baik" value="4"/>
                                            <label for="buku_wasiat_baik">Baik</label>
                                        </div>
                                        </div>
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="buku_wasiat" class="material-inputs" type="radio" id="buku_wasiat_cukup" value="3"/>
                                            <label for="buku_wasiat_cukup">Cukup</label>
                                        </div>
                                        </div>
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="buku_wasiat" class="material-inputs" type="radio" id="buku_wasiat_kurang" value="2"/>
                                            <label for="buku_wasiat_kurang">Kurang</label>
                                        </div>
                                        </div>
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="buku_wasiat" class="material-inputs" type="radio" id="buku_wasiat_buruk" value="1"/>
                                            <label for="buku_wasiat_buruk">Buruk</label>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Uji Petik</h4>
                                    <div class="row">
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="uji_petik"  class="material-inputs" type="radio" id="uji_petik_sangat_baik" value="10"/>
                                            <label for="uji_petik_sangat_baik">Sangat Baik</label>
                                        </div>
                                        </div>
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="uji_petik" class="material-inputs" type="radio" id="uji_petik_baik" value="8"/>
                                            <label for="uji_petik_baik">Baik</label>
                                        </div>
                                        </div>
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="uji_petik" class="material-inputs" type="radio" id="uji_petik_cukup" value="6" />
                                            <label for="uji_petik_cukup">Cukup</label>
                                        </div>
                                        </div>
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="uji_petik" class="material-inputs" type="radio" id="uji_petik_kurang" value="4" />
                                            <label for="uji_petik_kurang">Kurang</label>
                                        </div>
                                        </div>
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="uji_petik" class="material-inputs" type="radio" id="uji_petik_buruk" value="2" />
                                            <label for="uji_petik_buruk">Buruk</label>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Foto Presiden, Wakil Presiden dan Lambang Negara</h4>
                                    <div class="row">
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="foto_presiden"  class="material-inputs" type="radio" id="foto_presiden_sangat_baik" value="5"/>
                                            <label for="foto_presiden_sangat_baik">Ada Sesuai</label>
                                        </div>
                                        </div>
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="foto_presiden" class="material-inputs" type="radio" id="foto_presiden_baik" value="3"/>
                                            <label for="foto_presiden_baik">Ada Kurang Sesuai</label>
                                        </div>
                                        </div>
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <input name="foto_presidenfoto_presiden" class="material-inputs" type="radio" id="foto_presiden_cukup" value="1"/>
                                            <label for="foto_presiden_cukup">Belum Sesuai</label>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="submit"
                                            class="btn btn-success waves-effect waves-light mr-2">Submit</button>
                        </form>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            @include('layouts.footer')

        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
@endsection
@push('head')

@endpush
@push('scripts')
@endpush