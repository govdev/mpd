    <link href="{{asset('libs/chartist/dist/chartist.min.css')}}" rel="stylesheet">
    <link href="{{asset('js/pages/chartist/chartist-init.css')}}" rel="stylesheet">
    <link href="{{asset('libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.css')}}" rel="stylesheet">
    <link href="{{asset('libs/c3/c3.min.css')}}" rel="stylesheet">
    <link href="{{asset('extra-libs/css-chart/css-chart.css')}}" rel="stylesheet">
    <!-- Vector CSS -->
    <link href="{{asset('libs/jvectormap/jquery-jvectormap.css')}}" rel="stylesheet" />